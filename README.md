# OpenML dataset: Yolanda

https://www.openml.org/d/42705

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Universite Paris-Saclay  
**Source**: [original](https://automl.chalearn.org/data) - 20-02-2016  
**Please cite**: Guyon I. et al. (2019) Analysis of the AutoML Challenge Series 2015–2018. In: Hutter F., Kotthoff L., Vanschoren J. (eds) Automated Machine Learning. The Springer Series on Challenges in Machine Learning. Springer, Cham. https://doi.org/10.1007/978-3-030-05318-5_10

AutoML challenge 2014. 

Original task: regression.

Test and validation sets can be obtained on the Cha Learn website: https://automl.chalearn.org/data

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42705) of an [OpenML dataset](https://www.openml.org/d/42705). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42705/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42705/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42705/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

